$(".vi-lang").change(function() {
	var uri = $(this).attr('data-url');
    $.ajax({
        type: 'POST',
        url: '/admin/auth/check-email',
        data: {
            lang: $(this).attr('data-lang')
        },
        success: function(res) {
            if(res === 'TRUE') {
                window.location = uri;
            }
        }
    });
});