<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'welcome';
$route['default_controller'] = 'dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['account/forgot'] = 'account/auth/forgot';
$route['account/logout'] = 'account/auth/logout';
$route['account/auth']	= 'account/auth/index';

// ADMIN ROUTE

$route['department'] = 'admin/department/index';
$route['department/index'] = 'admin/department/index';
$route['department/create'] = 'admin/department/create';
$route['position'] = 'admin/position/index';
$route['position/index'] = 'admin/position/index';
$route['position/create'] = 'admin/position/create';
$route['employees'] = 'admin/employees/index';
$route['employees/index'] = 'admin/employees/index';
$route['employees/create'] = 'admin/employees/create';
$route['user'] = 'admin/user/index';
$route['user/index'] = 'admin/user/index';
$route['report'] = 'admin/report/index';
$route['report/list'] = 'admin/report/index';
$route['report/create'] = 'admin/report/create';
// $route['user/checkUsername/'] = 'admin/user/checkUsername/0';
// $route['user/checkUsername/(:num)'] = 'admin/user/checkUsername/$1';

// $route['admin/member'] 			= 'admin/member/index';
// $route['admin/member/']  		= 'admin/member/index/0';
// $route['admin/member/(:num)'] 	= 'admin/member/index/$1';

$route['aff/(:num)/(:any)'] = 'affiliate/aff/$1/$2';

$route['sitemap\.xml'] = 'home/sitemap';
