<?php
class Employees extends Admin_Controller {

    private $table_name = 'tbl_employees';
    private $primary = 'emp_id';

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('admin/admin_model');
        $this->load->library('user_agent');
        $this->data['active'] = 'employees';
    }
    public function index()
    {   
        $this->data['subtitle'] = 'Quản lý nhân viên';
        $options = $this->admin_model->get(array(
            'table'  => $this->table_name,
            'get_row' => false,
            'order_by' => $this->primary.' ASC',
        ));
        foreach($options as $k => $v) {
            $options[$k]->childs = new stdClass();
            $options[$k]->childs = $this->admin_model->get(array(
                'table' => 'tbl_department',    
                'where' => array('dep_id' => $v->department_id),
                'get_row' => true
            ));
        }
        $this->data['list'] = $options;
        $this->data['subview'] = 'admin/employees/index';
        $this->load->view('admin/admin_layout', $this->data);
    }
    public function create()
    {
        $this->data['subtitle'] = 'Thêm mới nhân viên';
        $list_dep = $this->admin_model->get(array(
            'table' => 'tbl_department',
        ));
        $list_pos = $this->admin_model->get(array(
            'table' => 'tbl_position',
        ));
        $this->data['dep'] = $list_dep;
        $this->data['pos'] = $list_pos;
        $this->data['subview'] = 'admin/employees/create';
        $this->load->view('admin/admin_layout', $this->data);
    }   
    public function edit($id)
    {
        $this->data['subtitle'] = 'Cập nhật thông tin nhân viên';
        $this->data['id'] = $id; 
        $list_dep = $this->admin_model->get(array(
            'table' => 'tbl_department',
        ));
        $list_pos = $this->admin_model->get(array(
            'table' => 'tbl_position',
        ));
        $this->data['dep'] = $list_dep;
        $this->data['pos'] = $list_pos;
        $this->data['list'] = $this->admin_model->get(array(
            'table' => $this->table_name,
            'where' => array($this->primary => $id), 
            'get_row' => true));
        
        $this->data['subview'] = 'admin/employees/edit';
        $this->load->view('admin/admin_layout', $this->data);
    }
    public function profile($id)
    {
        $this->data['subtitle'] = 'Thông tin nhân viên';
        $this->data['id'] = $id; 
        $options = $this->admin_model->get(array(
            'table' => $this->table_name,
            'where' => array($this->primary => $id), 
            'get_row' => true));
        $options->dep_name  =$this->admin_model->get(array(
            'table' => 'tbl_department',
            'select' => 'dep_name',    
            'where' => array('dep_id' => $options->department_id),
            'get_row' => true
        ))->dep_name;
        $options->pos_name  =$this->admin_model->get(array(
            'table' => 'tbl_position',
            'select' => 'pos_name',    
            'where' => array('pos_id' => $options->position_id),
            'get_row' => true
        ))->pos_name;
        $this->data['list'] = $options;
        $this->data['subview'] = 'admin/employees/profile';
        $this->load->view('admin/admin_layout', $this->data);
    }
    public function save()
    {
        $usid = $this->input->post('id');
        $id = 0;
        $data = $this->admin_model->array_from_post(array(
            'emp_name','emp_email','emp_phone','emp_address','emp_birthday','department_id','position_id'
        ));
        $date_tmp = date('Y-m-d',strtotime($data['emp_birthday'])); 
        $data['emp_birthday'] = $date_tmp; 
        $rs = $this->admin_model->save(array(
            'table' => $this->table_name,
            'data'  => $data,
            'primary' => $this->primary,
            'id'    => (isset($usid) ? $usid : NULL)
        ));
            redirect('employees');
        
    }
    public function delete($id=NULL) {
        $user = $this->session->userdata('web_manager');
        if($id == NULL) {
            $data = $this->input->post('cb', TRUE);
            if($user['role'] == '1' && $user['id'] != $id) 
                foreach($data as $value) 
                    $this->admin_model->delete(array(
                        'table'     => $this->table_name,
                        'key'       => $this->primary,
                        'value'     => $value
                    ));
        } else {
            if($user['role'] == '1' && $user['id'] != $id) 
                $this->admin_model->delete(array(
                    'table'     => $this->table_name,
                    'key'       => $this->primary,
                    'value'     => $id
                ));
        }
        redirect($this->agent->referrer());
    }

    
}
