<?php

class Report extends Admin_Controller {

    private $table_name = 'tbl_report';
    private $primary = 'rep_id';

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('admin/admin_model');
        $this->load->library('user_agent');
        $this->data['active'] = 'report';
    }
    public function index()
    {
        $this->data['subtitle'] = 'Danh sách báo cáo';
        $this->data['active'] = 'report';
        $options = $this->admin_model->get(array(
            'table'  => $this->table_name,
            'get_row' => false,
            'order_by' => $this->primary.' ASC',
        ));
        $this->data['list'] = $options;
        $this->data['subview'] = 'admin/report/index';
        $this->load->view('admin/admin_layout', $this->data);
    }
    public function create()
    {
        $this->data['subtitle'] = 'Tạo báo cáo';
        $this->data['subview'] = 'admin/report/create';
        $this->load->view('admin/admin_layout', $this->data);
    }   
    public function edit($id)
    {
        $this->data['subtitle'] = 'Cập nhật báo cáo';
        $this->data['id'] = $id;
        $options = $this->admin_model->get(array(
            'table'  => $this->table_name,
            'where' => array($this->primary => $id), 
            'get_row' => true,
            'order_by' => $this->primary.' ASC',
        ));
        $task= $this->admin_model->get(array(
            'table' => 'tbl_task',    
            'where' => array('rep_id' => $options->rep_id),
            'get_row' => false
        ));
        $this->data['list'] = $options;
        $this->data['task'] = $task;
        $this->data['subview'] = 'admin/report/edit';
        $this->load->view('admin/admin_layout', $this->data);
    }
    public function detail($id)
    {
        $this->data['subtitle'] = 'Chi tiết báo cáo';
        $this->data['id'] = $id;
        $options = $this->admin_model->get(array(
            'table'  => $this->table_name,
            'where' => array($this->primary => $id), 
            'get_row' => true,
            'order_by' => $this->primary.' ASC',
        ));
        $task= $this->admin_model->get(array(
            'table' => 'tbl_task',    
            'where' => array('rep_id' => $options->rep_id),
            'get_row' => false
        ));
        $emp = $this->admin_model->get(array(
            'table' => 'tbl_employees',
            'where' => array('emp_username' => $options->emp_username), 
            'get_row' => true));
        $emp->dep_name  =$this->admin_model->get(array(
            'table' => 'tbl_department',
            'select' => 'dep_name',    
            'where' => array('dep_id' => $emp->department_id),
            'get_row' => true
        ))->dep_name;
        $emp->pos_name  =$this->admin_model->get(array(
            'table' => 'tbl_position',
            'select' => 'pos_name',    
            'where' => array('pos_id' => $emp->position_id),
            'get_row' => true
        ))->pos_name;
        $this->data['list'] = $options;
        $this->data['emp'] = $emp;
        $this->data['task'] = $task;
        $this->data['subview'] = 'admin/report/detail';
        $this->load->view('admin/admin_layout', $this->data);
    }
    public function save()
    {
        $usid = $this->input->post('id');
        $id = 0;
        $data_rep = $this->admin_model->array_from_post(array(
            'rep_name'
        ));
        $data_task = $this->admin_model->array_from_post(array(
            'task_name','task_summary','task_result'
        ));
        $data_rep['emp_username'] = $this->session->userdata('web_manager')['username'];
        $rep = $this->admin_model->save(array(
            'table' => $this->table_name,
            'data'  => $data_rep,
            'primary' => $this->primary,
            'id'    => (isset($usid) ? $usid : NULL)
        ));
        if(isset($rep['id']) || $rep['id']!=NULL){
            if(!empty($data_task['task_name'])){
                foreach ($data_task['task_name'] as $key => $item) {
                    $task = array(
                        'task_name' => $item,
                        'task_summary' => $data_task['task_summary'][$key],
                        'task_result' => $data_task['task_result'][$key],
                        'rep_id' => $rep['id']
                    );
                    $task = $this->admin_model->save(array(
                        'table' => 'tbl_task',
                        'data'  => $task,
                        'primary' => 'task_id',
                        'id'    => (isset($usid) ? $usid : NULL)
                    ));   
                }
                redirect('report');
            }
        }
        
    }
    public function delete($id=NULL) {
        $user = $this->session->userdata('web_manager');
        //$super = $this->session->userdata('role');
        if($id == NULL) {
            $data = $this->input->post('cb', TRUE);
            if($user['role'] == '1' && $user['id'] != $id) 
                foreach($data as $value) 
                    //$this->admin_m->delete($this->table_name, $this->primary, $value);
                    $this->admin_model->delete(array(
                        'table'     => $this->table_name,
                        'key'       => $this->primary,
                        'value'     => $value
                    ));
        } else {
            if($user['role'] == '1' && $user['id'] != $id) 
                //$this->admin_m->delete($this->table_name, $this->primary, $id);
                $this->admin_model->delete(array(
                    'table'     => $this->table_name,
                    'key'       => $this->primary,
                    'value'     => $id
                ));
        }
        redirect($this->agent->referrer());
    }

    
}
