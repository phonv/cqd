<?php

class Department extends Admin_Controller {

    private $table_name = 'tbl_department';
    private $primary = 'dep_id';

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('admin/admin_model');
        $this->load->library('user_agent');
        $this->data['active'] = 'department';
    }
    public function index()
    {
        $this->data['subtitle'] = 'Phòng ban';
        $this->data['active'] = 'department';
        $options = array(
            'table'  => $this->table_name,
            'get_row' => false,
            'order_by' => $this->primary.' ASC',
        );
        $this->data['list'] = $this->admin_model->get($options);
        $this->data['subview'] = 'admin/department/index';
        $this->load->view('admin/admin_layout', $this->data);
    }
    public function create()
    {
        $this->data['subtitle'] = 'Thêm mới phòng ban';
        $this->data['subview'] = 'admin/department/create';
        $this->load->view('admin/admin_layout', $this->data);
    }   
    public function edit($id)
    {
        $this->data['subtitle'] = 'Cập nhật phòng ban';
        $this->data['id'] = $id;
        $this->data['list'] = $this->admin_model->get(array(
            'table' => $this->table_name,
            'where' => array($this->primary => $id), 
            'get_row' => true));
        
        $this->data['subview'] = 'admin/department/edit';
        $this->load->view('admin/admin_layout', $this->data);
    }
    public function filter($offset = 0)
    {
        $phone = $this->input->get('phone');
        $email = $this->input->get('email');
        $type = $this->input->get('member_type');

        $this->data['subtitle'] = 'Tìm kiếm thành viên';
        $options = array(
            'table'   => $this->table_name,
            'where'   => "((phone='$phone') OR (email='$email') OR (member_type='$type'))",
            'get_row' => false,
            'order_by' => $this->primary.' ASC',
            'offset'  => ($offset > 0 ? ($offset-1)*10 : 0),
            'total'   => 10
        );
        $this->data['old_phone']=$phone;
        $this->data['old_email']=$email;
        $this->data['old_type']=$type;
        $this->data['list'] = $this->admin_model->get($options);
        $this->data['pagination']  = $this->ad_pagination($options,'admin/member', 10);
        $this->data['subview'] = 'admin/member/filter';
        $this->load->view('admin/admin_layout', $this->data);
    }
    public function save()
    {
        $usid = $this->input->post('id');
        $affLink = $this->input->post('aff_link', TRUE);
        $id = 0;
        $data = $this->admin_model->array_from_post(array(
            'dep_name'
        ));
        $data['u_username'] = $this->session->userdata('web_manager')['username'];
        $rs = $this->admin_model->save(array(
            'table' => $this->table_name,
            'data'  => $data,
            'primary' => $this->primary,
            'id'    => (isset($usid) ? $usid : NULL)
        ));
            redirect('department');  
    }
    public function delete($id=NULL) {
        $user = $this->session->userdata('web_manager');
        //$super = $this->session->userdata('role');
        if($id == NULL) {
            $data = $this->input->post('cb', TRUE);
            if($user['role'] == '1' && $user['id'] != $id) 
                foreach($data as $value) 
                    //$this->admin_m->delete($this->table_name, $this->primary, $value);
                    $this->admin_model->delete(array(
                        'table'     => $this->table_name,
                        'key'       => $this->primary,
                        'value'     => $value
                    ));
        } else {
            if($user['role'] == '1' && $user['id'] != $id) 
                //$this->admin_m->delete($this->table_name, $this->primary, $id);
                $this->admin_model->delete(array(
                    'table'     => $this->table_name,
                    'key'       => $this->primary,
                    'value'     => $id
                ));
        }
        redirect($this->agent->referrer());
    }

    
}
