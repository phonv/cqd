<?php

class Member extends Admin_Controller {

    private $table_name = 'tbl_member';
    private $primary = 'id';

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('admin/admin_model');
        $this->load->library('user_agent');
        $this->data['active'] = 'member';
    }
    public function index($offset = 0)
    {
        $this->data['subtitle'] = 'Quản lý thành viên';
        $options = array(
            'table'  => $this->table_name,
            'where'     => array('domain' => $this->domain),
            'get_row' => false,
            'order_by' => $this->primary.' ASC',
            'offset'    => ($offset > 0 ? ($offset-1)*10 : 0),
            'total'     => 10
        );
        $this->data['list'] = $this->admin_model->get($options);
        $this->data['pagination']  = $this->ad_pagination($options,'admin/member', 10);
        $this->data['subview'] = 'admin/member/index';
        $this->load->view('admin/admin_layout', $this->data);
    }   
    public function edit($id)
    {
        $this->data['subtitle'] = 'Cập nhật thành viên';
        $this->data['id'] = $id;
        $this->data['mem'] = $this->admin_model->get(array(
            'table' => $this->table_name,
            'where' => array($this->primary => $id), 
            'get_row' => true));
        
        $this->data['subview'] = 'admin/member/edit';
        $this->load->view('admin/admin_layout', $this->data);
    }
    public function filter($offset = 0)
    {
        $phone = $this->input->get('phone');
        $email = $this->input->get('email');
        $type = $this->input->get('member_type');

        $this->data['subtitle'] = 'Tìm kiếm thành viên';
        $options = array(
            'table'   => $this->table_name,
            'where'   => "((phone='$phone') OR (email='$email') OR (member_type='$type'))",
            'get_row' => false,
            'order_by' => $this->primary.' ASC',
            'offset'  => ($offset > 0 ? ($offset-1)*10 : 0),
            'total'   => 10
        );
        $this->data['old_phone']=$phone;
        $this->data['old_email']=$email;
        $this->data['old_type']=$type;
        $this->data['list'] = $this->admin_model->get($options);
        $this->data['pagination']  = $this->ad_pagination($options,'admin/member', 10);
        $this->data['subview'] = 'admin/member/filter';
        $this->load->view('admin/admin_layout', $this->data);
    }
    public function profile($id)
    {
        $this->data['subtitle'] = 'Hồ sơ thành viên';
        $options = array(
            'table' => $this->table_name,
            'where' => array(
                'id' => $id,
                'domain' => $this->domain
            ),
            'get_row' => true
        );
        $this->data['member'] = $this->admin_model->get($options);
        // don hang dc thanh toan tu link affiliate
        $this->data['report'] = $this->admin_model->get(array(
            'table' => 'tbl_affiliate',
            'where' => array('member_id' => $this->data['member']->id),
            'get_row' => true
        ));
        // don hang cua thanh vien
        $this->data['listOrder'] = $this->admin_model->get(array(
            'table' => 'bill_customer',
            'where' => array('member_id' => $this->data['member']->id),
            'order_by' => 'date_create DESC',
            'get_row' => false
        ));
        $this->data['subview'] = 'admin/member/profile';
        $this->load->view('admin/admin_layout', $this->data);

    }
    public function export(){
        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle('Danh sách thành viên');
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'STT');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Họ và tên');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Email');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Số điện thoại');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Địa chỉ');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Dạng thành viên');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Ngày đăng kí');    
        // set Row
        $options = array(
            'table'  => $this->table_name,
            'where'     => array('domain' => $this->domain),
            'get_row' => false,
            'order_by' => $this->primary.' ASC',
        );
        $arr = $this->admin_model->get($options);
        $rowCount = 2;
        $stt = 1;
        foreach ($arr as $list) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $stt);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $list->name);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $list->email);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $list->phone);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $list->address);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $list->member_type);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $list->date_register);
            $rowCount++;
            $stt++;
        };
        $filename = "list_member-". date("Y-m-d-H-i-s").".xls";
        header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0'); 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');  
        $objWriter->save('php://output'); 
 
    }
    public function updateProfile()
    {
        // $values = $this->admin_model->array_from_post(array(
        //     'website'
        // ));
    }
    public function save()
    {
        date_default_timezone_set("Asia/Ho_Chi_Minh");
        $usid = $this->input->post('id');
        $affLink = $this->input->post('aff_link', TRUE);
        $id = 0;
        $data = $this->admin_model->array_from_post(array(
            'status', 'member_type'
        ));
        $rs = $this->admin_model->save(array(
            'table' => $this->table_name,
            'data'  => $data,
            'primary' => $this->primary,
            'id'    => (isset($usid) ? $usid : NULL)
        ));
        if($data['member_type'] =='affiliate'){
            $check = $this->checkAffiliate($rs['id']);
            if($check) {
                $this->admin_model->update(array(
                    'table' => 'tbl_affiliate',
                    'data' => array(
                        'aff_link'  => site_url().'aff/'.$rs['id'].'/'.$affLink,
                        
                    ),
                    'where' => array('member_id' => $usid)
                )); 
            } else {
                $this->admin_model->save(array(
                    'table' => 'tbl_affiliate',
                    'data' => array(
                        'member_id' => $rs['id'],
                        'aff_link'  => site_url().'aff/'.$rs['id'].'/'.$affLink,
                        'aff_click' => 0,
                        'number_order' => 0
                    ),
                    'primary' => 'aff_id',
                    'id' => NULL
                ));
            }
            redirect('admin/member');
            // redirect('admin/mail/notice/'.$usid);
        }
        else{
            redirect('admin/member');
        }
    }
    public function reset_pass($id)
    {
        date_default_timezone_set("Asia/Ho_Chi_Minh");
        $this->data['id'] = $id;
        $pass='a0123456789';
        $data =  array('password' => $pass);
        $this->admin_model->update(array(
            'table' => $this->table_name,
            'data'  => $data,
            'where' => array($this->primary => $id),
        ));
        redirect('admin/member');
    }
    public function delete($id=NULL) {
        $user = $this->session->userdata('web_manager');
        //$super = $this->session->userdata('role');
        if($id == NULL) {
            $data = $this->input->post('cb', TRUE);
            if($user['role'] == '1' && $user['id'] != $id) 
                foreach($data as $value) 
                    //$this->admin_m->delete($this->table_name, $this->primary, $value);
                    $this->admin_model->delete(array(
                        'table'     => $this->table_name,
                        'key'       => $this->primary,
                        'value'     => $value
                    ));
        } else {
            if($user['role'] == '1' && $user['id'] != $id) 
                //$this->admin_m->delete($this->table_name, $this->primary, $id);
                $this->admin_model->delete(array(
                    'table'     => $this->table_name,
                    'key'       => $this->primary,
                    'value'     => $id
                ));
        }
        redirect($this->agent->referrer());
    }

    public function updateStatus()
    {
        $id = $this->input->post('id', TRUE);
        $status = $this->input->post('status', TRUE);
        $this->admin_model->update(array(
            'table' => $this->table_name,
            'data'  => array('status' => $status),
            'where' => array('id' => $id)
        ));
        redirect($this->agent->referrer());
    }

    public function set_language() {
        $lang = $this->input->post('lang', TRUE);
        if($lang == NULL) {
            $lang = 'en';
        } 
        $ss_admin_lang = array(
            'ss_admin_lang' => $lang
        );
        $this->session->set_userdata($ss_admin_lang);
        $language = $this->session->userdata('ss_admin_lang');
        if($language == NULL) echo 'FALSE';
        else { 
            echo 'TRUE';
            //$this->load->library('user_agent');
            //redirect($this->agent->referrer());
        }
    }
    public function checkAffiliate($member_id)
    {
        $result = $this->admin_model->get(array(
            'table' => 'tbl_affiliate',
            'where' => array('member_id' => $member_id),
            'get_row' => true
        ));
        if($result == NULL) return false;
        else return true;
    }
    
}
