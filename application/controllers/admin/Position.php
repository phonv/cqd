<?php

class Position extends Admin_Controller {

    private $table_name = 'tbl_position';
    private $primary = 'pos_id';

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('admin/admin_model');
        $this->load->library('user_agent');
        $this->data['active'] = 'position';
    }
    public function index()
    {
        $this->data['subtitle'] = 'Chức vụ';
        $options = array(
            'table'  => $this->table_name,
            'get_row' => false,
            'order_by' => $this->primary.' ASC',
        );
        $this->data['list'] = $this->admin_model->get($options);
        $this->data['subview'] = 'admin/position/index';
        $this->load->view('admin/admin_layout', $this->data);
    }
    public function create()
    {
        $this->data['subtitle'] = 'Thêm mới chức vụ';
        $this->data['subview'] = 'admin/position/create';
        $this->load->view('admin/admin_layout', $this->data);
    }   
    public function edit($id)
    {
        $this->data['subtitle'] = 'Cập nhật chức vụ';
        $this->data['id'] = $id; 
        $this->data['list'] = $this->admin_model->get(array(
            'table' => $this->table_name,
            'where' => array($this->primary => $id), 
            'get_row' => true));
        
        $this->data['subview'] = 'admin/position/edit';
        $this->load->view('admin/admin_layout', $this->data);
    }
    public function save()
    {
        $usid = $this->input->post('id');
        $affLink = $this->input->post('aff_link', TRUE);
        $id = 0;
        $data = $this->admin_model->array_from_post(array(
            'pos_name'
        ));
        $data['u_username'] = $this->session->userdata('web_manager')['username'];
        $rs = $this->admin_model->save(array(
            'table' => $this->table_name,
            'data'  => $data,
            'primary' => $this->primary,
            'id'    => (isset($usid) ? $usid : NULL)
        ));
            redirect('position');
        
    }
    public function delete($id=NULL) {
        $user = $this->session->userdata('web_manager');
        //$super = $this->session->userdata('role');
        if($id == NULL) {
            $data = $this->input->post('cb', TRUE);
            if($user['role'] == '1' && $user['id'] != $id) 
                foreach($data as $value) 
                    //$this->admin_m->delete($this->table_name, $this->primary, $value);
                    $this->admin_model->delete(array(
                        'table'     => $this->table_name,
                        'key'       => $this->primary,
                        'value'     => $value
                    ));
        } else {
            if($user['role'] == '1' && $user['id'] != $id) 
                //$this->admin_m->delete($this->table_name, $this->primary, $id);
                $this->admin_model->delete(array(
                    'table'     => $this->table_name,
                    'key'       => $this->primary,
                    'value'     => $id
                ));
        }
        redirect($this->agent->referrer());
    }

    
}
