<?php 

class Dashboard extends Admin_Controller {

	function __construct() {
		parent::__construct();
		$this->data['active'] = 'dashboard';
		$this->data['sub_active'] = '';
	}

	public function index() {
		$this->data['subtitle'] = 'Dashboard';
		$this->data['subview'] 	= 'admin/dashboard/index';
		$this->load->view('admin/admin_layout', $this->data);
	}
}