<?php

class Login extends Frontend_Controller {
    private $table = 'tbl_member';
    private $id    = 'member_id';
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
    }

    public function index() {
        $this->data['subtitle'] = lang('login');
        $this->data['subview']  = 'account/login/index';

        $this->load->view('home/layout', $this->data);
    }
    public function loggedIn() {
        $user = $this->home_model->array_from_post(array(
            'email', 'password'
        ));

        $member = $this->home_model->get(array(
            'table'     => $this->table,
            'where'     => array(
                'member_email'  => $user['email'],
                'member_password'  => md5(md5(md5($user['password']))),
            ),
            'get_row'   => true
        ));
        if ($member->member_status == 1) {
            $data = array(
                'name'          => $member->member_name,
                'username'      => $member->member_username,
                'member_email'  => $member->member_email,
                'member_id'     => $member->member_id,
                'member_type'   => $member->member_type,
                'loggedin'      => TRUE
            );
            $this->session->set_userdata('namtrieu_member', $data);
            $member = $this->session->userdata('namtrieu_member');
            echo json_encode(array(
                'success' => true,
                'url' => site_url($this->web_lang),
                'username' => $member['username']
            ));
        } else {
            echo json_encode(array(
                'success' => false,
                'message' => 'Tài khoản này chưa được kích hoạt.'
            ));
        }
    }
    public function logout() {
        $this->session->unset_userdata('namtrieu_member');
    }
    public function checkEmail() {
        $email = trim($this->input->post('email', TRUE)," ");
        $check = $this->home_model->get(array(
            'table'     => $this->table,
            'where'     => array(
                'member_email'  => $email
            ),
            'get_row'   => true
        ));
        if($check->member_email != NULL) {
            echo json_encode(true);
        } else {
            echo json_encode(false);
        }
    }

    public function checkPassword() {
        $pass = $this->input->post('password', TRUE);
        $check = $this->home_model->get(array(
            'table'     => $this->table,
            'where'     => array(
                'member_password'  => md5(md5(md5($pass)))
            ),
            'get_row'   => true
        ));
        if($check->member_username != NULL) {
            echo json_encode(true);
        } else {
            echo json_encode(false);
        }
    }
}