<div class="row">
    <div class="col-md-6">
      <script>
        $(document).ready(function() {
          $('#birthday').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
          $('option[value="<? echo $list->department_id?>"]').attr('selected','selected');
          $('option[value="<? echo $list->position_id?>"]').attr('selected','selected');
        });
      </script>
        <div class="card card-primary">
              <!-- /.card-header -->
              <!-- form start -->
               <form class="form-horizontal" action="<?php echo site_url('admin/employees/save') ?>" method="post" accept-charset="utf-8">
                <div class="card-body">
                  <div class="form-group">
                    <input type="hidden" name="id" value="<? echo $id?>">
                    <label for="exampleInputEmail1">Tên nhân viên</label>
                    <input type="text" class="form-control" name="emp_name" placeholder="Employess name" value="<? echo $list->emp_name?>" />
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="text" class="form-control" name="emp_email" placeholder="Employees email" value="<? echo $list->emp_email?>"/>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Số điện thoại</label>
                    <input type="text" class="form-control" name="emp_phone" placeholder="Employees phone" value="<? echo $list->emp_phone?>"/>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Ngày sinh</label>
                    <input type="text" class="form-control" id="birthday" name="emp_birthday" data-inputmask="'alias': 'datetime','inputFormat': 'dd-mm-yyyy'" value="<? echo date('d-m-Y',strtotime($list->emp_birthday))?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Địa chỉ</label>
                    <input type="text" class="form-control" name="emp_address" placeholder="Employees address" value="<? echo $list->emp_address?>"/>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Phòng ban</label>
                    <select class="form-control" name="department_id">
                      <option>Chọn phòng ban</option>
                      <? foreach ($dep as $item) {
                       echo '<option value="'.$item->dep_id.'">'.$item->dep_name.'</option>';
                      }
                      ?> 
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Chức vụ</label>
                    <select class="form-control" name="position_id" >
                      <option>Chọn chức vụ</option>
                      <? foreach ($pos as $item) {
                       echo '<option value="'.$item->pos_id.'">'.$item->pos_name.'</option>';
                      }
                      ?>  
                    </select>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
    </div>
</div>
