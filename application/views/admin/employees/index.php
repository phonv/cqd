<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <p class="pull-left"><a class="addRole btn btn-info btn-flat" data-role="<?php echo $session_role ?>" href="<?php echo site_url('admin/employees/create') ?>"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a></p>
            </div>
            <div class="box-body">
                <div id="auth"></div>
                <form method="post" class="form-horizontal" accept-charset="utf-8" action="<?php echo site_url('admin/employees
                /delete') ?>" onsubmit="return confirm('Are you sure to delete ?')">
                    <table class="table table-hover" id="tblPage">
                        <thead>
                            <tr>
                                <th><input type="checkbox" id="selectAll" /></th>
                                <th>Tên nhân viên</th>
                                <th>Số điện thoại</th>
                                <th>Địa chỉ</th>
                                <th>Phòng ban</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if($list != NULL):
                            foreach($list as $key => $item) {
                        ?>
                            <tr>
                                <td><input type="checkbox" class="checkbox" name="cb[]" value="<?php echo $item->emp_id ?>" /></td>
                                <td><a href="<?php echo site_url('admin/employees/profile/'.$item->emp_id) ?>"><?php echo $item->emp_name ?></a></td>
                                <td><?php echo $item->emp_phone ?></td>
                                <td><?php echo $item->emp_address ?></td>
                                <td><? echo ($item->childs->dep_name)?></td>
                                <td>
                                    <a class="editRole btn btn-info btn-flat" data-role="<?php echo $session_role ?>" data-id="<?php echo ($item->emp_username!=null? $item->emp_username : '')?>"  href="<?php echo site_url('admin/employees/edit/'.$item->emp_id) ?>"><i class="far fa-edit"></i></a>
                                    <a class="deleteRow btn btn-danger btn-flat" data-role="<?php echo $session_role ?>" data-id="<?php echo ($item->emp_username!=null? $item->emp_username : '')?>" href="<?php echo site_url('admin/employees/delete/'.$item->emp_id) ?>" onclick="return confirm('Are you sure to delete ?');"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php } endif; ?>
                        </tbody>
                    </table>
                    <br />
                    <input type="submit" class="btn btn-danger btn-flat" value="Xoá các mục đã chọn" />
                </form>
            </div>
        </div>
    </div>
</div>