<div class="row">
    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-body ml-4">
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <div class="text-center">
                              <img class="profile-user-img img-fluid img-circle"
                              src="/public/dist/img/user4-128x128.jpg"
                              alt="User profile picture">
                            </div>

                            <h3 class="profile-username text-center"><? echo $list->emp_name?></h3>

                            <p class="text-muted text-center"><? echo $list->pos_name?></p>

                            <ul class="list-group list-group-unbordered mb-3">
                                <li class="list-group-item">
                                    <b>Phòng ban</b> <a class="float-right"><? echo $list->dep_name?></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Email</b> <a class="float-right"><? echo $list->emp_email?></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Số điện thoại</b> <a class="float-right"><? echo $list->emp_phone?></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Địa chỉ</b> <a class="float-right"><? echo $list->emp_address?></a>
                                </li>
                            </ul>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>