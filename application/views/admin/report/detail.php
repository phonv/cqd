<div class="col-12">
	<div class="row">
		<div class="col-8">
			<div class="card pb-5">
              <div class="card-header">
              	<div class="row">
              		<div class="col-6 pt-3 pl-5">
              			<p class="text-left font-weight-bold">VNPT Lai Châu</p>
              		</div>
              		<div class="col-6 pt-2 text-center">
              			<p class="font-weight-bold pb-0 mb-0">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</p>
              			<p class="pt-0 mt-0 font-weight-bold" style="font-size: 14px">Độc lập - Tự do - Hạnh phúc</p>
                		<p class="float-right pr-5"><i>Ngày <? echo date('d-m-yy', strtotime($list->rep_createAt))?></i></p>
              		</div>
          		</div>
                <h3 class="text-center"><? echo $list->rep_name ?></h3>
                <div class="row pt-3">
	                <p class="font-weight-bold pl-4 pr-5">Họ tên: <? echo $emp->emp_name?></p>
	                <p class="font-weight-bold pl-4 pr-5">Đơn vị công tác: <? echo $emp->dep_name?></p>
	                <p class="font-weight-bold pl-4 pr-5">Chức vụ: <? echo $emp->pos_name?></p>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                		<th>STT</th>
                      	<th>Công việc</th>
                      	<th>Kết quả</th>
                      	<th>Nội dung</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<? foreach($task as $key => $item){?>
                    <tr>                
                    	<td><? echo $key+1?></td>      
                		<td><? echo $item->task_name?></td>
                      	<td><span class="tag tag-success"><? echo $item->task_result?></span></td>
                      	<td><? echo $item->task_summary?></td>
                    </tr>
                	<? } ?>
                  </tbody>
                </table>
              </div>

              <!-- /.card-body -->
            
            </div>
        </div>
	</div>
</div>