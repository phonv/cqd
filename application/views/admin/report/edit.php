<div class="col-12">
  <form class="form-horizontal" action="<?php echo site_url('admin/report/save') ?>" method="post" accept-charset="utf-8">
    <div class="row">
      <div class="col-md-3">
        <div class="card card-primary">
          <div class="card-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Tiêu đề báo cáo</label>
              <input type="text" class="form-control" name="rep_name" value="<? echo $list->rep_name?>" placeholder="Report title" />
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Người tạo</label>
              <input type="text" class="form-control" name="u_username" value="<? echo $list->emp_username?>" disabled/>
            </div>
            <div class="form-group">
              <label for="exampleInputFile">Ngày tháng</label>
              <input type="text" class="form-control" name="date" value="<? echo date('d/m/Y',strtotime($list->rep_createAt))?>" disabled/>  
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-9">
        <div class="card card-primary">
          <div class="card-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Nội dung báo cáo</label>
            </div>
            <div class="col-12">
              <div id='item' class="card card-primary">
                <? foreach($task as $item) {?>
                  <div class="card-body">
                    <div class="row">
                      <div class="col-4">
                        <div class="form-group">
                          <label for="exampleInputPassword1">Công việc</label>
                          <input type="text" class="form-control" name="task_name[]" value="<? echo $item->task_name ?>" />
                          <label for="exampleInputPassword1">Kết quả</label>
                          <input type="text" class="form-control" name="task_result[]" value="<? echo $item->task_result ?>"/>
                        </div>
                      </div>
                      <div class="col-8">
                        <div class="form-group">
                          <label for="exampleInputFile">Chi tiết công việc</label>
                          <textarea type="text" class="form-control" name="task_summary[]" rows="4"><? echo $item->task_summary ?></textarea>  
                        </div>
                      </div> 
                    </div>
                  </div>
                <? } ?>
              </div>
              <div class="float-right">
                <a id='addItem' class="btn btn-primary">&nbsp;&nbsp;+&nbsp;&nbsp;</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
<script>
  $(function() {
    $('#addItem').click(function() {
      let item = '<div class="card-body"><div class="row"><div class="col-4"><div class="form-group">';
      item += '<label for="exampleInputPassword1">Công việc</label>';
      item += '<input type="text" class="form-control" name="task_name[]" />';
      item += '<label for="exampleInputPassword1">Kết quả</label>';
      item += '<input type="text" class="form-control" name="task_result[]" /></div></div>';
      item += '<div class="col-8"><div class="form-group"><label for="exampleInputFile">Chi tiết công việc</label>';
      item += '<textarea type="text" class="form-control" name="task_summary[]" rows="4"></textarea></div></div></div>';
      item += '<div class="rmItem btn btn-danger">&nbsp;&nbsp;-&nbsp;&nbsp;</div></div>';
      $('#item').append(item);
    });
    $("#item").on('click', '.rmItem', function(){
      $(this).parent().remove();
    });
  });
</script>