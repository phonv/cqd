<div class="row">
    <div class="col-lg-12">
        <p class="pull-left"><a class="btn btn-info btn-flat" href="<?php echo site_url('admin/report/create') ?>"> <i class="glyphicon glyphicon-plus"></i> Thêm mới</a></p>
    </div>
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-body">
                <div id="auth"></div>
                <form action="<?php echo site_url('admin/report/delete') ?>" method="=post">
                    <table class="table table-hover" id="tblUser">
                        <thead>
                            <tr>
                                <!-- <th><input type="checkbox" id="selectAll" /></th> -->
                                <th>Báo cáo</th>
                                <th>Người tạo</th>
                                <th>Ngày tháng</th>
                                <th>Tùy chọn</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($list as $item) { ?>
                            <tr>
                                <!-- <td><input type="checkbox" class="checkbox" name="cb[]" value="<?php echo $user->id ?>" /></td> -->
                                <td><?php echo $item->rep_name ?></td>
                                <td><a href="<?php echo site_url('admin/report/detail/'.$item->rep_id) ?>"><?php echo $item->emp_username ?></a></td>
                                <td><?php echo date('d-m-y',strtotime($item->rep_createAt))?></td>
                                <td>
                                    <a class="editRole btn btn-info btn-flat" href="<?php echo site_url('admin/report/edit/'.$item->rep_id) ?>" data-id="<?php echo $item->emp_username?>" data-role="<?php echo $session_role ?>"><i class="far fa-edit"></i></a>
                                    <a class="deleteRow btn btn-danger btn-flat" href="<?php echo site_url('admin/report/delete/'.$item->rep_id) ?>" data-id="<?php echo $item->emp_username?>" data-role="<?php echo $session_role ?>"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>