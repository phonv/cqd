<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-body">
                <form id="formMember" method="post" name="form1" action="<?php echo site_url("admin/user/save") ?>" class="form-horizontal" accept-charset="utf-8">
                    <?php if ($member == NULL) { ?>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nhân viên</label>
                            <div class="col-md-7">
                                <select class="form-control" name="emp_id" id="emp_id">
                                  <? foreach ($emp as $item) {
                                   echo '<option value="'.$item->emp_id.'">'.$item->emp_name.'</option>';
                                  }
                                  ?> 
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tên đăng nhập</label>
                            <div class="col-md-7">
                                <input class="form-control" name="username" type="text" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Mật khẩu</label>
                            <div class="col-md-7">
                                <input class="form-control" name="password" type="password" />
                                <br>
                                <button class="btn btn-default btn-flat" type="button" onclick="return randomString();">Tạo mật khẩu ngẫu nhiên</button>
                                <span class="showPass hidden"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Vị trí</label>
                            <div class="col-md-7">
                                <select class="form-control" name="role">
                                    <option value="1">Super admin</option>
                                    <option value="2">Manager</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10">
                                <div class="pull-right">
                                    <input type="submit" name="submit" class="btn btn-primary" value="Lưu" />
                                    <a href="<?php echo site_url('admin/user'); ?>" title="Về trang quản lý" class="btn btn-default">&larr; Về trang quản lý</a>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <input type="hidden" name="id" value="<?php echo $id ?>" />
                        <div class="form-group">
                            <label class="control-label col-md-3">Nhân viên</label>
                            <div class="col-md-7">
                                <input class="form-control" name="firstname" value="<?php echo $member->firstname ?>" type="text" disableds/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tên đăng nhập</label>
                            <div class="col-md-7">
                                <input class="form-control" name="username" required="required" value="<?php echo $member->username ?>" type="text" disabled/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Mật khẩu</label>
                            <div class="col-md-7">
                                <input class="form-control" name="password" type="password" />
                                <br>
                                <button class="btn btn-default btn-flat" type="button" onclick="return randomString();">Tạo mật khẩu ngẫu nhiên</button>
                                <span class="showPass hidden"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Vị trí</label>
                            <div class="col-md-7">
                                <select class="form-control" name="role">
                                    
                                    <option value="1" <?php echo ($member->role == 1 ? "selected='selected'" : "") ?>>Super admin</option>
                                    <option value="2" <?php echo ($member->role == 2 ? "selected='selected'" : "") ?>>Manager</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10">
                                <div class="pull-right">
                                    <input type="submit" name="submit" class="btn btn-primary" value="Cập nhật" />
                                    <a href="<?php echo site_url('admin/user'); ?>" title="Về trang quản lý" class="btn btn-default">&larr; Về trang quản lý</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function randomString() {
        var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
        var string_length = 8;
        var randomstring = '';
        for (var i = 0; i < string_length; i++) {
            var rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum, rnum + 1);
        }
        document.form1.password.value = randomstring;
        $(".showPass").removeClass('hidden');
        $(".showPass").text(randomstring);
    }
    var site = location.protocol + '//' + location.host;
    // console.log
    $(document).ready(function() {
        $('label[class="error"]').attr('ac','4');
        $("#formMember").validate({
            rules: {
                emp_id: {
                    remote: {
                        url: site + '/admin/user/checkUser',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            username: function () {
                                return $('#emp_id option:selected').val();
                            }
                        }
                    }
                },
                username: { 
                    required: true,
                    remote: {
                        url: site + '/admin/user/checkUsername',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            username: function () {
                                return $('#formMember :input[name="username"]').val();
                            }
                        }
                    }
                },
                password: { 
                    required: true,
                }
            },
            messages: {
                username: { 
                    required: "Bạn phải nhập tài khoản",
                    remote: "Tài khoản đã tồn tại"
                },
                password: { 
                    required: "Bạn phải nhập mật khẩu",
                    // remote: "Mật khẩu không hợp lệ"
                },
                emp_id: {
                    remote: "Phải chọn nhân viên chưa có tài khoản"
                }
            },
        });
    })
</script>
