<div class="row">
    <div class="col-lg-12">
        <p class="pull-left"><a class="addRole btn btn-info btn-flat" href="<?php echo site_url('admin/user/edit') ?>" data-role="<?php echo $session_role ?>"> <i class="glyphicon glyphicon-plus"></i> Thêm mới</a></p>
    </div>
    <div class="col-lg-12">
        <div class="box box-primary">
            <div class="box-body">
                <div id="auth"></div>
                <form action="<?php echo site_url('admin/user/delete') ?>" method="=post">
                    <table class="table table-hover" id="tblUser">
                        <thead>
                            <tr>
                                <!-- <th><input type="checkbox" id="selectAll" /></th> -->
                                <th>Họ tên</th>
                                <th>Tên đăng nhập</th>
                                <th>Phân quyền</th>
                                <th>Tùy chọn</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($list as $user) { ?>
                            <tr>
                                <!-- <td><input type="checkbox" class="checkbox" name="cb[]" value="<?php echo $user->id ?>" /></td> -->
                                <td><?php echo $user->firstname ?></td>
                                <td><?php echo $user->username ?></td>
                                <td><?php echo ($user->role == 1 ? 'Super admin' : 'Manager')?></td>
                                <td>
                                    <a class="editRole btn btn-info btn-flat" href="<?php echo site_url('admin/user/edit/'.$user->id) ?>" data-id="<?php echo $user->username?>" data-role="<?php echo $session_role ?>"><i class="far fa-edit"></i></a>
                                    <a class="deleteRow btn btn-danger btn-flat" href="<?php echo site_url('admin/user/delete/'.$user->username) ?>" data-id="<?php echo $user->id?>" data-role="<?php echo $session_role ?>"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>