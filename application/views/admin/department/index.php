<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                <p class="pull-left"><a class="btn btn-info btn-flat" href="<?php echo site_url('admin/department/create') ?>"><i class="glyphicon glyphicon-plus"></i> Thêm mới</a></p>
            </div>
            <div class="box-body">
                <form method="post" class="form-horizontal" accept-charset="utf-8" action="<?php echo site_url('admin/department/delete') ?>" onsubmit="return confirm('Are you sure to delete ?')">
                    <table class="table table-hover" id="tblPage">
                        <thead>
                            <tr>
                                <th><input type="checkbox" id="selectAll" /></th>
                                <th>Tên phòng ban</th>
                                <th>Người tạo</th>
                                <th>Thời gian</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if($list != NULL):
                            foreach($list as $item) {
                        ?>
                            <tr>
                                <td><input type="checkbox" class="checkbox" name="cb[]" value="<?php echo $item->dep_id ?>" /></td>
                                <td><?php echo $item->dep_name ?></td>

                                <td>
                                    <?php echo $item->u_username ?>
                                </td>
                                <td>
                                    <?php echo date('d-m-Y',strtotime($item->dep_createAt)) ?>
                                </td>
                                <td>
                                    <a class="btn btn-info btn-flat" href="<?php echo site_url('admin/department/edit/'.$item->dep_id) ?>"><i class="far fa-edit"></i></a>
                                    <a class="btn btn-danger btn-flat" href="<?php echo site_url('admin/department/delete/'.$item->dep_id) ?>" onclick="return confirm('Are you sure to delete ?');"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        <?php } endif; ?>
                        </tbody>
                    </table>
                    <br />
                    <input type="submit" class="btn btn-danger btn-flat" value="Xoá các mục đã chọn" />
                </form>
            </div>
        </div>
    </div>
</div>