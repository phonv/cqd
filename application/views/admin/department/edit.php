<div class="row">
    <div class="col-md-6">

        <div class="card card-primary">
              <!-- /.card-header -->
              <!-- form start -->
               <form class="form-horizontal" action="<?php echo site_url('admin/department/save') ?>" method="post" accept-charset="utf-8">
                <div class="card-body">
                  <div class="form-group">
                    <input type="hidden" name="id" value="<? echo $list->dep_id?>">
                    <label for="exampleInputEmail1">Tên phòng ban</label>
                    <input type="text" class="form-control" name="dep_name" placeholder="Department name" value="<? echo $list->dep_name?>" />
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Người tạo</label>
                    <input type="text" class="form-control" name="u_username" value="<? echo $list->u_username?>" disabled/>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Ngày tháng</label>
                        <input type="text" class="form-control" name="date" value="<? echo date('d-m-Y',strtotime($list->dep_createAt))?>" disabled/>
                        
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
    </div>
</div>
<script type="text/javascript">
</script>