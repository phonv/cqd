<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>AdminLTE 3 | Dashboard 3</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="/public/plugins/fontawesome-free/css/all.min.css">
  <!-- IonIcons -->
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/public/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <script src="/public/plugins/jquery/jquery.min.js"></script>


<!-- <script src="/public/plugins/jquery/jquery.min.js"></script> -->
<script src="/public/js/jquery.validate.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/public/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="/public/plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="/public/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="/public/plugins/moment/moment.min.js"></script>
<script src="/public/js/jquery.inputmask.min.js"></script>
<!-- <script src="/public/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script> -->
<!-- date-range-picker -->
<script src="/public/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="/public/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="/public/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="/public/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- AdminLTE App -->
<script src="/public/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/public/dist/js/demo.js"></script>
<style type="text/css">
  .error{
    color: red;
  }
</style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge">15</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button"><i
            class="fas fa-th-large"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="/public/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">VNPT</span>
    </a>
    <?php $user = $this->session->userdata('web_manager')?>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex mr-0">
        <div class="col-3">
          <div class="image">
            <img src="/public/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
          </div>
        </div>
        <div class="col-9">
          <div style="overflow: visible" class="info">
            <div class="row">
              <div class="col-5">
                <a href="#" class="d-block" id='username' user='<? echo $user['username']?>'><? echo $user['username']?></a>
              </div>
              <div class="col-6">
                <div class="pull-right">
                  <a href="<? echo site_url('account/logout')?>" class="btn btn-info">Sign Out</a>
                </div>
              </div>              
            </div>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link <? echo($active==1 ? 'active' : '')?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
          
          </li>
          <li class="nav-item">
            <a href="<? echo site_url('employees')?>" class="nav-link <? echo($active=='employees' ? 'active' : '')?>">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Nhân viên
                <span class="right badge badge-danger">New</span>
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link <? echo($active=='department' ? 'active' : '')?>">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Phòng ban
                <i class="fas fa-angle-left right"></i>
                <span class="badge badge-info right">6</span>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<? echo site_url('department/create')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<? echo site_url('department')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách phòng ban</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link <? echo($active=='position' ? 'active' : '')?>">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Chức vụ
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<? echo site_url('position/create')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<? echo site_url('position')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link <? echo($active=='report' ? 'active' : '')?>">
              <i class="nav-icon fas fa-tree"></i>
              <p>
                Báo cáo công việc
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<? echo site_url('report/create')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm mới</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<? echo site_url('report/list')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách báo cáo</p>
                </a>
              </li>
              
            </ul>
          </li>
          <li class="nav-item">
            <a href="<? echo site_url('user')?>" class="nav-link <? echo($active=='user' ? 'active' : '')?>">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Tài khoản
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            
          </li>
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><? if(isset($subtitle)) echo ($subtitle!='' ? $subtitle : 'VNPT');?></h1>
          </div><!-- /.col -->
          
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <?php $this->load->view($subview, TRUE) ?>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.4
    </div>
  </footer>
</div>
<script type="text/javascript">

      $(document).ready(function() {
        $(".addRole").click(function() {
            var role = $(this).attr('data-role');
            // alert(role);
            console.log(role);
            if(role!='admin') {
                var html = '<div class="alert alert-warning alert-dismissible">';
                    html+= '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                    html+= '<strong>Cảnh báo!</strong> Bạn không đủ quyền thực hiện hành động này. Bạn chỉ có thể chỉnh sửa thông tin của chính mình.'
                    html+='</div>';
                $('#auth').html(html);
                return false;
            }
        });
        $(".editRole").click(function() {
            var role = $(this).attr('data-role');
            var user_name= $(this).attr('data-id');
            let username = $('#username').attr('user');
            if(role!='admin' && user_name != username) {
                var html = '<div class="alert alert-warning alert-dismissible">';
                    html+= '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                    html+= '<strong>Cảnh báo!</strong> Bạn không đủ quyền thực hiện hành động này. Bạn chỉ có thể chỉnh sửa thông tin của chính mình.'
                    html+='</div>';
                $('#auth').html(html);
                return false;
            }
        });
        $(".deleteRow").click(function() {
            var role = $(this).attr('data-role');
            var user_name = $(this).attr('data-id');
            let username = $('#username').attr('user');
            if(role==='admin' && user_name != username) {
                return confirm("Bạn có chắc chắn muốn xóa không?");
            } else {
                var html = '<div class="alert alert-warning alert-dismissible">';
                    html+= '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>';
                    html+= '<strong>Cảnh báo!</strong> Bạn không đủ quyền thực hiện hành động này.'
                    html+='</div>';
                $('#auth').html(html);
                return false;
            }
        });
      });
  </script>
</body>
</html>
