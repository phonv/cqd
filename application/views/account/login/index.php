
<section class="member-login">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-lg-3"></div>
            <div class="col-lg-6 col-md-6">
                <h3>Đăng nhập tài khoản</h3>
                <form id="loginForm" class="form-horizontal" method="post">
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" placeholder="Email" />
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Mật khẩu" />
                    </div>

                    <div class="form-group">
                        <div class="pull-left">
                            <input type="submit" class="btn btn-primary" value="Đăng nhập" />
                        </div>
                        <div class="pull-right">
                            <p>Chưa có tài khoản ? <a href="<?php echo site_url($GLOBALS['lang_code'].'/register') ?>">Đăng ký tại đây </a></p>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-3 col-lg-3"></div>
        </div>
    </div>
</section>