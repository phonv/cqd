<?php 

class Admin_Model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    public function update_parent($param)
    {
        $this->db->set(array('cat_parent' => 0));
        $this->db->where('cat_parent', (int)$param['id']);
        $this->db->update($param['table']);
    }
    public function update_serv_parent($param)
    {
        $this->db->set(array('service_parent' => 0));
        $this->db->where('service_parent', (int)$param['id']);
        $this->db->update($param['table']);
    }
    public function get_billBydate($param) {
        $this->db->distinct();
        if (isset($param['select'])) {
            $this->db->select($param['select']);
        } else {
            $this->db->select();
        }
        if (isset($param['where'])) {
            $this->db->where($param['where']);
        }
        if (isset($param['order_by'])) {
            $this->db->order_by($param['order_by']);
        }
        if(isset($param['group_by'])) {
            $this->db->group_by($param['group_by']);
        }
        if(isset($param['join'])&& $param['join']==true) {
            $this->db->join('bill_detail','bill_customer.bill_id = bill_detail.bill_id');
        }
        if (isset($param['limit']) && isset($param['start'])) {
            $this->db->limit($param['limit'], $param['start']);
        }
        if (isset($param['limit'])) {
            $this->db->limit($param['limit']);
        } 
        if (isset($param['total']) && isset($param['offset'])) {
            return $this->db->get($param['table'], $param['total'], $param['offset'])->result();
        } else {
            if (isset($param['get_row']) && $param['get_row'] == true) {
                return $this->db->get($param['table'])->row();
            } else {
                return $this->db->get($param['table'])->result();
            }
        }
    }
    public function getEmailByMember($id)
    {
        $result = parent::get(array(
            'table' => 'tbl_member',
            'where' => array('id' => $id),
            'get_row' => true
        ));
        return $result;
    }
    public function check_Email($email)
    {
        $result = parent::get(array(
            'table' => 'tbl_member',
            'where' => array('email' => $email),
            'get_row' => true
        ));
        return $result;
    }
    public function check_username($username)
    {
        $result = parent::get(array(
            'table' => 'tbl_member',
            'where' => array('username' => $username),
            'get_row' => true
        ));
        return $result;
    }

    public function save_nav_order($pages) {
        if (count($pages)) {
            foreach ($pages as $order => $page) {
                if ($page['item_id'] != '') {
                    $data = array(
                        'nav_order' => $order,
                        'nav_parent' => (int) $page['parent_id']
                    );
                    $this->db->set($data)->where('nav_id', $page['item_id'])->update('tbl_navigation');
                }
            }
        }
    }
}