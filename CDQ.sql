-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost
-- Thời gian đã tạo: Th4 12, 2020 lúc 07:05 PM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `CDQ`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_department`
--

CREATE TABLE `tbl_department` (
  `dep_id` int(11) NOT NULL,
  `dep_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `u_username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dep_createAt` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_department`
--

INSERT INTO `tbl_department` (`dep_id`, `dep_name`, `u_username`, `dep_createAt`) VALUES
(2, 'Kinh tế ', 'admin', '2020-04-08'),
(4, 'Phòng kinh doanh ', 'admin', '2020-04-08'),
(5, 'Kỹ thuật ', 'admin', '2020-04-08'),
(7, 'Lãnh đạo', 'admin', '2020-04-08');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_employees`
--

CREATE TABLE `tbl_employees` (
  `emp_id` int(11) NOT NULL,
  `emp_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_phone` int(11) DEFAULT NULL,
  `emp_birthday` date DEFAULT NULL,
  `emp_address` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_employees`
--

INSERT INTO `tbl_employees` (`emp_id`, `emp_name`, `emp_email`, `emp_phone`, `emp_birthday`, `emp_address`, `emp_username`, `department_id`, `position_id`) VALUES
(1, 'Mạnh', 'ajsdhjh@ns.cn', 192039039, '1998-09-21', 'Like Chow', 'manh', 7, 4),
(2, 'Phố', 'uqyusa.cm', 92130709, '1998-10-24', 'NĐ', 'admin', 7, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_position`
--

CREATE TABLE `tbl_position` (
  `pos_id` int(11) NOT NULL,
  `pos_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `u_username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pos_createAt` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_position`
--

INSERT INTO `tbl_position` (`pos_id`, `pos_name`, `u_username`, `pos_createAt`) VALUES
(1, 'Giám đốc', 'admin', '2020-04-08'),
(4, 'Phó GĐ', 'admin', '2020-04-09');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_report`
--

CREATE TABLE `tbl_report` (
  `rep_id` int(11) NOT NULL,
  `rep_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `emp_username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rep_createAt` date DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_report`
--

INSERT INTO `tbl_report` (`rep_id`, `rep_name`, `emp_username`, `rep_createAt`) VALUES
(10, 'Bao cao 1', 'manh', '2020-04-12'),
(11, 'Bao cao 1', 'admin', '2020-04-12');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_task`
--

CREATE TABLE `tbl_task` (
  `task_id` int(11) NOT NULL,
  `task_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `task_summary` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `task_result` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rep_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_task`
--

INSERT INTO `tbl_task` (`task_id`, `task_name`, `task_summary`, `task_result`, `rep_id`) VALUES
(5, 'Cong viec 1', 'ajsk', 'Hoanthanh', 10),
(6, 'Cong viec 2', 'jskdhakhd', 'Chua xong', 10),
(7, 'Cong viec 1', '', 'Hoanthanh', 11),
(8, 'Cong viec 2', '', 'Chua xong', 11),
(9, 'Cong viec 3', 'jkdjs', 'Dang lam', 11);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `firstname`, `image`, `username`, `password`, `role`) VALUES
(1, 'Phố', '', 'admin', '57f231b1ec41dc6641270cb09a56f897', 1),
(6, 'Mạnh', NULL, 'manh', '57f231b1ec41dc6641270cb09a56f897', 2);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tbl_department`
--
ALTER TABLE `tbl_department`
  ADD PRIMARY KEY (`dep_id`);

--
-- Chỉ mục cho bảng `tbl_employees`
--
ALTER TABLE `tbl_employees`
  ADD PRIMARY KEY (`emp_id`);

--
-- Chỉ mục cho bảng `tbl_position`
--
ALTER TABLE `tbl_position`
  ADD PRIMARY KEY (`pos_id`);

--
-- Chỉ mục cho bảng `tbl_report`
--
ALTER TABLE `tbl_report`
  ADD PRIMARY KEY (`rep_id`);

--
-- Chỉ mục cho bảng `tbl_task`
--
ALTER TABLE `tbl_task`
  ADD PRIMARY KEY (`task_id`);

--
-- Chỉ mục cho bảng `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `tbl_department`
--
ALTER TABLE `tbl_department`
  MODIFY `dep_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `tbl_employees`
--
ALTER TABLE `tbl_employees`
  MODIFY `emp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `tbl_position`
--
ALTER TABLE `tbl_position`
  MODIFY `pos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `tbl_report`
--
ALTER TABLE `tbl_report`
  MODIFY `rep_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `tbl_task`
--
ALTER TABLE `tbl_task`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
